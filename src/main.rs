use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;

#[derive(Deserialize)]
struct AddParams {
    a: Option<i32>,
    b: Option<i32>,
}

fn is_prime(n: i32) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..=(n as f64).sqrt() as i32 {
        if n % i == 0 {
            return false;
        }
    }
    true
}

async fn add(params: web::Query<AddParams>) -> impl Responder {
    match (params.a, params.b) {
        (Some(a), Some(b)) => {
            let sum = a + b;
            if b < 0 {
                HttpResponse::Ok().body(format!("{} + ({}) = {}\n{} is a prime? {}", a, b, sum, sum, is_prime(sum)))
            }
            else {
                HttpResponse::Ok().body(format!("{} + {} = {}\n{} is a prime? {}", a, b, sum, sum, is_prime(sum)))
            }
        },
        _ => HttpResponse::BadRequest().body("Missing or invalid parameters!"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[actix_rt::test]
    async fn test_add_1() {
        let app = actix_web::test::init_service(App::new().route("/", web::get().to(add))).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/?a=2&b=3")
            .to_request();
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_success());
        let body = actix_web::test::read_body(resp).await;
        assert_eq!(body, web::Bytes::from_static(b"2 + 3 = 5\n5 is a prime? true"));
    }

    #[actix_rt::test]
    async fn test_add_2() {
        let app = actix_web::test::init_service(App::new().route("/", web::get().to(add))).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/?a=-2&b=3")
            .to_request();
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_success());
        let body = actix_web::test::read_body(resp).await;
        assert_eq!(body, web::Bytes::from_static(b"-2 + 3 = 1\n1 is a prime? false"));
    }

    #[actix_rt::test]
    async fn test_add_3() {
        let app = actix_web::test::init_service(App::new().route("/", web::get().to(add))).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/?a=2.2&b=3")
            .to_request();
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_client_error());
        let body = actix_web::test::read_body(resp).await;
        assert_eq!(body, web::Bytes::from_static(b"Query deserialize error: invalid digit found in string"));
    }

    #[actix_rt::test]
    async fn test_add_4() {
        let app = actix_web::test::init_service(App::new().route("/", web::get().to(add))).await;
        let req = actix_web::test::TestRequest::get()
            .uri("/?a=-2&b=-3")
            .to_request();
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_success());
        let body = actix_web::test::read_body(resp).await;
        assert_eq!(body, web::Bytes::from_static(b"-2 + (-3) = -5\n-5 is a prime? false"));
    }
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(add)))
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
