# Yl954 Individual Project 2
This web service provides an HTTP service endpoint for calculating the sum of two integers and determining whether the sum is a prime number. This service is implemented through an asynchronous function called add, which accepts the query parameters (a and b) via an HTTP GET request and attempts to add them. Both parameters are optional, if not provided or invalid, the service will return an error response. If both arguments are valid, they are added and the program checks whether the result is prime.

## Author
Yuanzhi Lou

## Demo Video
The demo video is called ```DemoVideo.mp4``` and is in the root directory of this repository

## Install Docker [link](https://docs.docker.com/engine/install/ubuntu/)
1. Run the following command to uninstall all conflicting packages
```
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
```
2. Set up Docker's apt repository
```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```
3. Install the Docker packages
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
4. Verify
```
sudo docker run hello-world
```

## Create Rust Actix Web Application
1. ```cargo new actix_web_app```
2. Add Actix-web to ```Cargo.toml```
```
[dependencies]
actix-web = "4.5.1"
```
3. Replace the content of ```src/main.rs``` with Actix web server code
![](screenshots/main_code.png)
4. Write test mod for testing
![](screenshots/test_code.png)

## Create Dockerfile
**I add ```RUN cargo test -v``` during the docker build process to test the service**
```
# Use an official Rust image
FROM rust:1.75 as builder

# Create a new empty shell project
RUN USER=root cargo new indiv2
WORKDIR /indiv2

# Copy the manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This is a dummy build to get the dependencies cached
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build for release
RUN rm ./target/release/deps/indiv2*
RUN cargo build --release

# test
RUN cargo test -v

# Final stage
FROM debian:bookworm-slim
COPY --from=builder /indiv2/target/release/indiv2 .
ENV ROCKET_ADDRESS=0.0.0.0
CMD ["./indiv2"]
```

## Build Docker Image
```sudo docker build -t indiv2 .```
![](screenshots/docker_build.png)

## Delete Docker Image
1. Get image ID by ```sudo docker images```
2. Delete image by ```sudo docker rmi <image_ID>```

## Create and run Container Locally
```sudo docker run (-d) -p 8080:8080 indiv2```\
Note: ```-d: run in the background```
![](screenshots/docker_run.png)
![](screenshots/docker_web.png)

## Start or Stop or Delete Container
1. View all containers: ```sudo docker ps -a```
2. Start: ```sudo docker start container_id```
3. Stop: ```sudo docker stop container_id```
4. Delete: ```sudo docker rm container_id```

## Test the Service
1. Test using ```cargo test -v```
![](screenshots/test_cargo.png)
2. Test using curl
![](screenshots/test_curl.png)

## CI/CD
Create the ```.gitlab-ci.yml``` file. The Docker in Docker (docker:dind) service is used here to allow another Docker daemon to be started inside the GitLab Runner's Docker container. By encapsulating all the build and test tasks within the Dockerfile, the CI/CD pipeline only needs to build and run the Docker container. If any test cases failed, the pipeline will provide the error message.
```
image: docker:25.0.3

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - test

test:
  stage: test
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t indiv2 .
    - docker run -d -p 8080:8080 indiv2
    - docker ps -a
```